# Make 10 (Readme) #

### Description ###

Simple program to calculate all possible calculations between 4 numbers (0-9)
Uses addition, subtraction, multiplication and division
Aim of the program is to determine whether the number 10 can be made from 4 numbers

### Modes ###

1. Show all possibilities (-1 to represent invalid operations)
2. Show all possibilities except invalid operations
3. Show whether the four numbers can create 10