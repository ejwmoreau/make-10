import sys

print "** 1 -> All possibilities; 2 -> All +ve nums; 3 -> Only 10's **"
mode = int(raw_input("Which mode? "))

def calculate(a, b):
    a = int(a)
    b = int(b)

    if (a < 0 or b < 0):
        if (mode == 1):
            return [-1, -1, -1, -1, -1, -1]
        else:
            return []

    c = [(a + b), (a - b), (b - a), (a * b)]

    if (a % b == 0):
        c.append((a / b))
    elif (mode == 1):
        c.append(-1)

    if (b % a == 0):
        c.append((b / a))
    elif (mode == 1):
        c.append(-1)

    return c

nums = []
nums.append(raw_input("1st Number: "))
nums.append(raw_input("2nd Number: "))
nums.append(raw_input("3rd Number: "))
nums.append(raw_input("4th Number: "))

sets = [[nums[0], nums[1], nums[2], nums[3]],
        [nums[0], nums[2], nums[1], nums[3]],
        [nums[0], nums[3], nums[1], nums[2]]]

answers = []

for i in range(0, 3):
    ans1 = calculate(sets[i][0], sets[i][1])
    ans2 = calculate(sets[i][2], sets[i][3])
    for j in ans1:
        for k in ans2:
            answers.append(calculate(j, k))

for i in answers:
    if (mode == 1):
        print i
    elif (mode == 2):
        if (len(i) != 0):
            print i
    else:
        for j in i:
            if (j == 10):
                print True
                sys.exit()

